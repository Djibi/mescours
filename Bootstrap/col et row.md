# COL ET ROW

Il est interdit d'avoir des col sans row et des row sans col.La col est toujour a l' interieur d'une row.

```html
<div class="container">
    <div class="row">
        <div class="col-3 offset-3">&nbsp;</div> 
        <div class="col-3 offset-3>&nbsp;</div> 
    </div> 
```

- La page est composer de 12 colones qui doivent être completer par les offset.

- Pour faire apparaître des couleurs sans contenu il faut ajouter des éspaces insécables : `&nbsp;`  
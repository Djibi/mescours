# bootstrap container

Lorsque que l'ont fait un site avec bootstrap on doit mettre un container qui englobe tout le code de la page.

Il y a deux types de container :

`< div class="container">`:  centre le site au milieu de la page.

`< div class="container-fluid >`: le site prend tout la page 


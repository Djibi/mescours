# Introduction

C'est un language de prise de note et de note de code:

#: pour les titres

##: pour les sous-tires

``: pour une ligne de code

```` ```: pour plusieur ligne de codes

**: pour écrire en gras

_ _: pour écrire en italic

`>`: pour écrire une citation

 `[lien](.com)`:pour faire un lien vers une page

*Il se peut qu'il faille laisser des éspaces tout dépend de l'IDE*.
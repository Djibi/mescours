# Connexion au nas

Pour se connecté au nas on utilise un service qui est fourni par le fabriquant du nas:

- Dans un navigateur taper Quickconnect.to.

- Mettre mots de passe et pseudo fourni par le résponsable du serveur.

**Attention si il y a 10 tentative avec un mauvais pseudo on est bannis du nas pour des raisons de securité**.
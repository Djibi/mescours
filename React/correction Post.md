# correction Post

le post est composé de deux component `MyForm` pour le formulaire et `MyMessage` pour le message d'erreur

Dans le message on va affiché si il y une erreur et si on a réussi a crée l'objet on affiche sont uid

Quand on fait un poste en backend il faut afficher les uid

 on a 2 input le nom de l'hotel et le nombre de chambre de l'hotel mais on aurait pu avoir une piscine
 
 dans ce cas la il faut faire un input avec une chek-box

On a crée une variable `sending` pour savoir si on envoi quelque chose ou pas

Dans notre exemple on vaut que useEffect se joue quand submit se met a jour 

```javascript

  function MyForm({setMessage}) {
    const [name, setName] = React.useState('');
    const [roomNumbers, setRoomNumbers] = React.useState('');

    const [sending, setSending] = React.useState(false);

    React.useEffect(() => {
      async function postHotel({name, roomNumbers}) {
        if (!sending) {
          return;
        }
        setMessage('post in progress');
        try {
          const response = await axios.post(api, {name, roomNumbers});
          setMessage(`La chambre créée a l'uid : ${response.data.id}`);
        } catch (e) {
          setMessage(e.data.message);
        }
        setSending(false);
      }

      postHotel({name, roomNumbers});
    }, [sending]);

```

```javascript

  function handleSubmit(event) {
      event.preventDefault();
      if (!name || !roomNumbers) {
        setMessage('ERROR missing data');
        setSending(false);
        return;
      }
      setSending(true);
    }

    function handleName(event) {
      const {value} = event.target;
      setName(value);
    }

    function handleRoomNumbers(event) {
      const {value} = event.target;
      setRoomNumbers(value);
    }

    return <form onSubmit={handleSubmit}>
      <label htmlFor="name">name</label>
      <input
          value={name}
          onChange={handleName}
          type="text" id="name"/>

      <label htmlFor="roomNumbers">roomNumbers</label>
      <input
          value={roomNumbers}
          onChange={handleRoomNumbers}
          type="number" id="roomNumbers"/>
      <button type="submit">Valider</button>
    </form>;
  }

  function MyMessage({message}) {
    return <pre>{message}</pre>;
  }

  function App() {
    const [message, setMessage] = React.useState('');
    return <>
      <MyForm setMessage={setMessage}/>
      <MyMessage message={message}/>
    </>;
  }

  ReactDOM.render(<App/>, document.getElementById('root'));
</script>

```
![poste12](img/post12.PNG)

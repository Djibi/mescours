# affichage conditionnel

On crée des component que l'ont veut affiché dans le navigateur les un aprés les autres.

![Un](img/Un.PNG)

si on ne fait pas `JsonStringify` on a un défaut car Réact ne sait pas faire l'interpolation d'un objet mais de string et de number

mais on peut le remplacé par value car c'est un number

![value](img/value.PNG)



Dans notre exemple ont veut que si la valeur est 1 on affiche `Je suis un`

2 solution:

![2](img/2.PNG)

![22](img/22.PNG)

Ou 2éme solution:

![case](img/case.PNG)

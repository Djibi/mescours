# les props et astuce destructuration

Comment utiliser la destructuration pour simplifier  l'utilisation de jsx.

On crée 2 constante:

![const](img/const.PNG)

On crée une `const props` qui veut dire proportionalité

![props](img/props.PNG)

le resultat

![divprops](img/divProps.PNG)

Si on remplace children par toto le navigateur n'afffiche plus rien car jsx sait qu'il doit un avoir un nom bien précis pour exister. 

![toto](img/toto.PNG)

Grace a props on peut passer a notre div toute les infos que l'on veut

## typer les props

![typeprops](img/typeprops.PNG)

**il faut faire attention de faire une interpolation pour age c'est a dire le mettre entre {} sinon il affiche un string**

![typeprops2](img/typeprops2.PNG)

## spécifier les types des props

On peut retrouver c'est infos sur le repo : `github.com/facebook/prop-types`

prop-types est une librairie qui permet de spécifier les types des component

![proptypes](img/proptypes.PNG)

La librairie va aller verifier ce qu'il y a dans l'objet propTypes

![hello](img/hello.PNG)

`.isRequired`: veut dire indispensable si on oublie de le mettre il affiche une erreur dans la console.

Si on met un string a la place d'un boolean ('' a la place de {}) il affiche une erreure. 


# choisir une classe grace a une props

On veut pouvoir personaliser les boxe quand on les utilises:

1er solution on peut faire des ajoutes directement dans les Box mais ce n'est pas la meilleure facons.

La meilleure solution est d'avoir un attribut 'size' qui un props et de pouvoir y faire les modifs, cela facilite aussi au niveau de la documentaion.
![box](img/box.PNG)

![large](img/large.PNG)

![small](img/small.PNG)

Pour cela il faut ajouté size dans les prop et dans className:

![size](img/size.PNG)

dans la console

![bg](img/bg4.PNG)

Si on retire une size dans une des box on se retrouve avec `undifined` dans la console.Dans les fonctions on peut mettre des valeurs par defauts c'est a dire si size est undifined

on met :` function Box({className, size = '', ...props})` la console affiche juste un espace.

Si on veut changer la nomenclature de size il faut faire les modifs dans les parametres de la fonction et dans la div:

```javascript

 function Box({className, size = '', ...props}) {
    let sizeClass;
    if (size === 'big') {
      sizeClass = 'large';
    }
    if (size === 'middle') {
      sizeClass = 'medium';
    }
    if (size === 'little') {
      sizeClass = 'small';
    }

    return (
        <div
            className={`container ${className} ${sizeClass}`}
            {...props}
        />
    );
  }

```
et faire les modif dans const exampleDiv:

![div5](img/div5.PNG)

On peut aussi multiplier les conbinaisons de classe dans les props.

# La prop style

On utilise les styles pour récupérer les information d'un template. On montre ici comment

on récupérent les attributs qui sont dans un props un peu comme une fonction de callback en js.

Dans notre exemple on prend la balise 'style':

![big](img/big.PNG)

on obtient rien dans le navigateur car on est en react et non en css.on doit utiliser l'interpolation:

![box6](img/box6.PNG)

on obtient:

![box7](img/box7.PNG)

Style fait partie des proprietes spéciale qui sont managé par react donc il prend style pour un objet et non pour un string 

dans la console:

![color](img/color.PNG)

Si on crée un objet 'Toto':

![seb](img/seb.PNG)

![toto1](img/toto1.PNG)

Il affiche cela car 'toto' n'est pas une props reconnue par react donc si ont veut créer nos propre props il faut faire des modif les parametres de la fonction.

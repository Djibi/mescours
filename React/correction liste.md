# correction liste

L'exercise consiste a faire une liste d'hotel

il faut penser a 'seter' `hotel.map` avec un tableaux vide a `useState` pour intialiser la variable hotel
![liste1](img/liste1.PNG)

Quand on a toute la liste `setState` affiche ok

![liste2](img/liste2.PNG)

On a 2 component `liste` sert a affiché la liste des collections et le component `state` sert a affiché l'etat au moment de 
l'operation.

![liste3](img/liste3.PNG)

quand on fait un map en react il faut toujour passer une key au objet que l'on a.

## focus sur useEffect

![ook](img/ook.PNG)

`React.useEffect` est un tableaux de dépendence des effets, quand on met une varible dans ce tableaux 

sa va demander de relancer a chaque que la valeur change donc risque de tourné en boucle.

`useEffect` et `useState` sont des **react-ook** c'est ce qu'on appel de la programmation fonctionnel

on crée un bouton `handleClik` avec une fonction `handleClick` et quand on click sur `reaload` sa change sa valeur

pour relancé reload en boucle on crée une variable et on le met dans le tableaux vide de useEffect

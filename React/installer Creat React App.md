# Installer creat react app

Cette outil permet d'installer la toute dérniere version de react.Il y a aussi un serveur de build

pour faire des action npm pour lancer des script pour faire des live reload, la compilation final du serveur pour l'envoyer en

production.

pour l'installtion on fait npx et on donne un ordre pour crée une application 

![npx](img/npx.PNG)
on copie-colle le lien ci-dessus dans WS, on l'appel promo4
on ne peut pas mettre des lettres en majuscules

On va 'linker' notre projet avec notre repo gitlab:

- Crée un new project

- on va utiliser la commande `gitinit` pour d'abord crée le projet

Dans WS => open => Webstorm project => creat-react-app-promo3 => open

![open](img/open.PNG)

ensuite il faut le lié a gitlab

![add](img/add.PNG)

puis vcs=> git => push=> on obtient `define remote` sa veut dire on défini a quel endroit on s'accroche dans gitlab ou github

on y clone le repo gitlab
![ok](img/ok.PNG)

puis push => start dans le terminal

## CRA presentation

On installe bootstrap dans react:

Dans le terminal `npm install --save bootstrap`

Dans le dossier on a un index.js on ne doit pas y toucher il sert a initialiser l'application

`<React.StrictMode/>` sont des balises qui servent a éviter le maximum d'erreur quand on code

Par contre on a le droit de touché au fichier App.js

![p](img/p.PNG)

on supprime  <p> et <a>

React étant importé dans un node-Module il faut faire`import React from 'react'`;
 
La nouveauté c'est que l'ont peut importé une feuille de style `import './App.css'`
 
On met une majuscule au fichier react 

Pour importer bootstrap:

![css3](img/css3.PNG)

**En react quand l' import commence par un . on va cherché dans le dossier dans le quel ont est mais si il n'y a pas de point
on va cherché dans les node-modules** 

![bt3](img/bt3.PNG)

![bt2](img/bt2.PNG)

## CRA créer component

On crée un dossier qu'on appel components on y met tout les component

une page entiere n'est pas un component mais c'est un menu un header un footer

![début](img/début.PNG)

on va utiliser des classe react au lieu d'utiliser des fonctions 

![render](img/render.PNG)

component est une classe qui nous vient de react

![10](img/11.PNG)

export default : veut dire exporte par defaut

![11](img/11.PNG)

![12](img/12.PNG)

## import CRA

Suite a la mise a jour de react les `classe` vu précedement sont abandonné ainsi que `extend` et `render`

Donc on peut récupérer les fonctions faite dans les exo en mettant les import

![21](img/21.PNG)

![22](img/23.PNG)

![20](img/20.PNG)

![24](img/24.PNG)

![25](img/25.PNG)

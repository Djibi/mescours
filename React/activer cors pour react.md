# activer cors pour react

Pour changer d'api

Il faut commencé par faire `watch` et `serve` dans le terminal

![api](img/api.PNG)
On colle l'url ci-dessus dans notre projet react a la place de l'url de `jsonplaceholder` qui l'api en ligne

Le resultat affiche un defaut le cors n'est pas autorisé il y a un probléme de compatibilité

les navigateurs mettent en place un systéme applé le cross-origin c'est a dire on ne veut pas qu'un site avec une url A 

va prendre des data sur un site avec une url B sauf si celui-ci l'autorise

Dans le terminal du projet react:

- `cd serveur`

- `npm i -S cors`

- `npm i @types/cors`

ensuite on applique le cors sur l'application en ajoutant `app.use(cors());`
`import cors from 'cors';`

# gérer les états proprement

Pour gérer les etats ont a créer une fonction `setState` mais c'est une mauvaise pratique car il faudrait la répeter dans chaque fonction

on la remplace par `React.useState()` qui l'appel automatiquement

Le `rederApplication()` qui est dans `setState` est problématique car c'est nous qui décidons quand react doit se mettre a jours

![supprimé](img/supprimé.PNG)

On veut gérer le nombre d'event(nb de click) le noms de l'event, et affiché le noms de la personne qui est dans l'input 1 :

![const2](img/const2.PNG)

On initialise `React.useState` avec l'etat de démarrage de notre component donc on peut supprimé `state`.

`React.useState` renvoi un tableaux, le 1er élément s'appel state et renvoie aussi setState   

![stat](img/stat.PNG)

Dans la fonction `handleInputChange` `setState` fonctionne comme un 'put' il prend et il remplace donc il faut faire

une destructuration ou l'on récupérent l'ancienne valeur de state et on la met a jour.

![final](img/final1.PNG)

![final2](img/final2.PNG)

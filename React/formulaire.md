# formulaire

![formulaire](img/formulaire.PNG)

On crée un conponent `MyForm`

Un formulaire déclenche une action c'est un poste vers une autre page d'un site internet

On y met 2 bouton 'valider' et 'annuler'

le fait d'appuyer sur valider s'appel la 'submition' un `onSubmit`

Comme il y a deux boutons le formulaire pense qu'il y deux bouton de submition même si ont appuie sur 'annuler' donc il faut précisé `type:"buton"`
et `type:"submite"` pour le bouton 'valider'

On rajout un `input` avec un `label` c'est un norme pour le web et lier les deux avec `htmlFor` et `id`

On fait une fonction `handleSubmit`

A ce niveau quand on appuie sur valider sa rechage la page , il faut supprimé ce rechargement de page

En rajoutant `event.preventDefault` veut dire empêche le comportement par defaut et sa empêche le rechargement de la page  

![formulaire2](img/formulaire2.PNG)

![formulaire3](img/formulaire3.PNG)


## récupérer les valeurs du formulaire

On va récuperer la valeur de state en cliquant sur submit:

En faisant un `console.log(state)` dans `handleSubmit` on obtient :

![coucou](img/coucou.PNG)

Au debut on a un number mais`event.target` renvoi toujours un string donc on fait un ``parseInt

![parse2](img/parse2.PNG)

C'est dans `handleSubmit` que plus tard on va faire des appel serveurs pour lui envoyé des data.

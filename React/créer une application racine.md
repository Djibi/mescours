# créer une application racine

Dans ce cours ont apprend a gérer les evenements en réact :

On definie une variable 'root':

![root](img/root.PNG)

qu'on passait en paramétre a la fin

![root2](img/root2.PNG)

comme on utilise les 2 a chaque fois ont peut mettre les deux en un a la fin

![root3](img/root3.PNG)

Ensuite on va avoir une application principale qui va contenire toute les autres applications c'est le component de base

on l'appel `App`
![App1](img/App1.PNG)
On remplace la variable ReactDom par la fonction ci-dessus et on appel la fonction

on obient:
![App2](img/App2.PNG)


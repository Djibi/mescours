# passer des fonction en props

On va voir comment manipuler des données repartis entre plusieur component

Dans notre exemple on veut ajouté un titre <h1> au dessus de `<calc>` et qui affiche le bouton cliqué.

On veut que l'affichage soit dans le component `App` mais la valeur passe par `Calc` et `Touche`

![clique](img/clique.PNG)

On veut que `oneClick` change d'état donc ajoute `setState`

On veut que quand on appuie sur `oneClick` sa appel `setState` qui lui passe par `calc` puis va dans touche

![set](img/set.PNG)

Donc on fait un seul `setSate` dans l' APP principale

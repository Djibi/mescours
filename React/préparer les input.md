# préparer les inputs

![formulaire4](img/formulaire4.PNG)

![formulaire5](img/formulaire5.PNG)


On met en place le systéme des `useState`

pour l'age on met `null` pour signalé  faire un signalement a la personne si elle oublie de remplire cette case.

On veut qu'a chaque fois qu'on fait une modife dans input react le prend en compte pour sa on rajoute `value`

Ensuite il faut rajouté la prop `onChange` pour faire des changements qui prend en parametre les infos qui sont dans input

et qui va changer les state en fonction de ces infos on appel cette fonction `handleInput`

## récupérer les valeur d'un input

On va voir les infos que l'ont récpérent dans la `handleInput`. En faisant un console log

on récupére un `SyntheticEvent` dans la console mais elle n'affiche que des valeur 'null'

donc ont fait un `console.dir(event.target)`, ensuite il faut faire un `setState` pour pouvoir changer la valeur de `Age`

Dans `InputName` on a l'id

On fait de la déstructuration dans `handleInput` pour avoir les deux valeurs

![des](img/des.PNG)


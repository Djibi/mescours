# use effect

On va voir comment gerer le monde extérieur a react par exemple quand on cherche des donnée sur un serveur ou des cookies

il faut dire a react qu'il va devoir gérer des données qui viennent de l'exterieure pour sa on va utiliser une api en ligne

`jsonplaceholder.com`  Dans Ws on crée une `const api` on y colle l'url de l'api

Pour faire des get en react on utilise la librairie `axios`. Dans notre exemple on veut aller chercher un user et l'affiché

on fait une props de `uid`

on peut diminué une ligne de code:

![4](img/4.PNG)

![5](img/5.PNG)

On met `axios.get` mais on peut aussi avoir `axios.put` `axois.post`...

react ne veut pas qu'on fasse de `DisplayUser` qui est un component une fonction `async` donc il faut que l'on crée une autre fonction

a l'interieur : `getUserByUid`

**En react quand on fait une fonction async on doit y mettre un return vide, au cas ou il y a un probléme  **

Comme on va cherché des donnnées hors react on doit mettre `React.useEffect`

![6](img/6.PNG)

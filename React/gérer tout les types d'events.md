# gérer tout le types d'events

on créer une fonction handleEvent pour gérer tout type d'événement 

`lastEventType` va nous donné le dernier élément survenu

`event`c'est l'evenement dans le navigateur au moment ou l'on fait un onClick. `event` est de type clique

donc on ecrit `event.type` cela veut dire que c'est de type 'clique'

`onMoussEnter` quand on passe avec la souri sa donne le derniere evenement

`onMousLeave` a chaque fois que l'on passe la souri on incrémente les nombres de cliques

![mouse](img/mouse.PNG)

![mousse2](img/mouse2.PNG)

# input value event

On va voir comment gérer un evenement avec un input:

![input](img/input.PNG)

![inputResult](img/inputResult.PNG)

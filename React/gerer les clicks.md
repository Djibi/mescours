# gerer les clicks

On va voir comment on gérent les clicks dans une application react

on commence par créer le bouton ou l'on va cliqué avec la balise <bouton>

![bouton](img/bouton.PNG)
Ensuite en html il faut expliqué au template que react veut cliké sur le bouton pour cela on utilise la foncion `onClick`

`onClick` veut applé une fonction donc on créer cette fonction a l'interieure de la fonction App, ca ne marche pas autrement

on l'appel `handleClick()`  veut dire prend en charge un click

![click](img/click.PNG)

**attention on ne met pas les () a handleClick dans la balise < bouton >**

ce que l'on passe en parametre c'est la fonction et non l'execution de cette fonction 

On peut aussi passer des informations complémentaires a handleClick par exemple `event` pour avoir les evenements dans la console

![event](img/event.PNG)

![event1](img/event1.PNG)

Dans notre exemple on a besoin d'avoir un nombre de click on créer une varible qu'on appel state

En react on travail avec des imutables c'est a dire a chaque que l'on click sur un objet on le remplace , on a pas le droit de changer un objet mais on peut le remplacé par un autre.

On créer une fonction `setState c'est le nouvel objet qui va remplacé state. On utilise Object.assign pour remplacer un objet

En react on a ce qu'on appel le virtualDom a chaque nouvel interpolation il faut faire un nouveau rendering avec `renderApplication`

![state](img/state.PNG)

![count](img/count.PNG)

La fonction `setState` sert a decidé si on fait une modif pour eviter les effets de bord c'est a dire non désiré, que les gens 

modifies des donnes sans autorisation.


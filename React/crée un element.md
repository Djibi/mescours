# crée un element
React est une librairie qui sert a gerer le virtuel Dome

Dans ce cour on va créer notre premiere élément du virtuelDom en écrivant exceptionnellement dans une page html.

En react on fait essentiellement du js et non du typescript

![react](img/react.PNG)

on fait`React.version` pour avoir la version de react. On import react grace au lien ci-dessous:

![import](img/import.PNG)

pour aller chercher l'element du dome `id = "root"`, on utilise `document.getelementById`
`document`: est la page ou l'ont est acctuellement

`getElementById`: veut dire va chercher un element du dom par son id, dans notre exemple 'root'

`children`: On y met le contenue de la div

le `${ }` permet d'aller chercher la valeur de l'élément entre accollade

ensuite on importe reactDOM avec le deuxieme lien. ReactDom sert a faire un affichage (ramder) en y ajoutant

`exempleDiv et root`
 

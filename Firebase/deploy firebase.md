# deploy firebase

Dans le terminal on installe  les firebase-tools qui sont des outils pour faciliter la création et le déploiment d'outil.

Dans le terminal:

- `npm install -g firebase tools`

- créer un fichier html a la racine pour lier les pages entre elles avec la balise `<a>`

- taper `firebase login` dans le terminal pour reconnaitre le compte firebase

- Entrer pour choisir une option puis entrer

- Taper `firebase init hosting` 

- Repondre ./pour la 1er question et No aux 2 questions suivante

- Faire `firebase deploy` 

Info supplémentaire:
``` html
{
  "hosting": {
    "public": "./",
    "ignore": [
      "firebase.json",
      "**/.*",
      "**/node_modules/**"
    ]
  }
}

```
Ceci est une fiche firebase.json:

./ veut dire la racine

"ignore" veut dire tout les fichiers qu'ont ne va pas envoyer sur le site.

Si ont y met le "node modules" on doit sotir les `dist` et `js` de bootstrap

et jquery pour les mettres dans un dossier vendors que l'on créer dans le dossier assets.

En cas de modification du site aprés l'avoir envoyé sur firebase il faut refaire un `firebase deploy` puis `ctrl+R` sur 
firebase.

 



















































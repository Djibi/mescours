# organiser les fonctions firebase

- Créer un dossier backend

- dans le terminal tapé : `cd backend/`

- dans backend créer un dossier `onUserCreated` puis `cd onUserCreated/` dans le terminal

- ds le terminal `firebase init functions`

Quand firebase est dejas initialiser a la racine il risque de créer les fonctions n'importe ou, donc on neutralise l'insta

en rajoutant bis a la fin.Meme chose pour firebase.json

![bis](img/bis.PNG)

- dans le terminal `firebase init functions`

- on choisi le projet. on reponds oui au question

![src](img/src.PNG)

faire les modif dans `index.ts` et `package.json`

![index1](img/index1.PNG)

![json](img/json.PNG)

## node créer une fonction avec firebase

on doit avoir juste un gitignore a la racine avec un .idea dedans

on met un dossier par fonctionnalité 

![trello](img/trello.PNG)

On créer un dossier et dans le terminal on se met dans le dossier pour `firebase init`

cocher tout les points en jaune.
![terminal](img/terminal.PNG)

repondre yes aux questions. 

![yes](img/yes.PNG)

ensuite on parametre l'emulator

![emulator](img/emulator.PNG)

- puis entrer et yes au chargement de l'emulator

on a besoin de l'emulator pour faire du backend

on sécurise le deploy: le deploy c'est quand on envoi une fonction en ligne, il demande si on ne veut pas supprimer les autres

![test2](img/test2.PNG)

![index2](img/index2.PNG)

### ajouter express

faire `npm i -S express` dans le dossier ou l'on veut utiliser expresse

![app](img/app.PNG)

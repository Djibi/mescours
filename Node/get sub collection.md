# get sub collection

On veut récuperer un hotel avec les infos d'une room et les ref et uid.

```typescript

export async function getHostelByIdWithAllRooms(hostelId: string): Promise<HotelModel> {

    const hotelToFind: HotelModel = await getHostelById(hostelId);

    const roomsCollectionInHotel: CollectionReference = refHotels
        .doc(hostelId)
        .collection('rooms');

    const roomsCollectionInHotelSnap: QuerySnapshot = await roomsCollectionInHotel.get();
    const roomsRef: string[] = [];
    roomsCollectionInHotelSnap.forEach(roomRef => roomsRef.push(roomRef.data().uid));
    const rooms = await getAllRoomsByUids(roomsRef);
    hotelToFind.roomsData = rooms;
    return hotelToFind;
}

```
**important**

**On ne peut mettre admin.initializeApp qu'a un seul endroit, comme on l'utilise dans plusieur fichier en même on le met dans le fichier 
principale avant qu'on appel room.service et hotel.service**

![get sub collection](img/get%20sub%20collection.PNG)

Donc on obtient bien l'hotel d'origine et les rooms.

![result-sub-collection](img/result-sub-collection.PNG)

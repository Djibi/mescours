# promise all

On a un hotel avec des rooms ou on a des uid et des ref et on veut récuperer les infos qui sont dans les uid et les ref pour ca
on utilise `promise All` pour envoyer et recevoir tout les `promise` en même temps.
 
![app-promiseAll](img/app-promiseAll.PNG)

on crée une 'dumb route' pour faire un essai on crée un [] vide ou l'on met
des uid

![app.promise-All](img/app.promise-All.PNG)

![function.promise-All](img/function.promise-All.PNG)

on fait une fonction qui prend en paramtere un array avec tout les uid que l'on veut récuperer

on fait un map pour pointer vers l'uid que l'on a choisi, et on le transforme en promesse avec `.get`
 

# node delete

Les navigateur ne savent faire que des Get, donc pour utilser les autres verbes ont utilises un logiciel appelé Postman
  
Dans le navigateur taper Postman => Download the App

Postman est une application creé pour faire des verbes

Dans WB copier le localhost qui est dans la ligne de commandes et le coller dans postman dans **enter requset URL**

 ```typescript

let users = [
     {
    id: 1,
    lastName: 'toto';
    age: 34,
    }, 
    {
    id : 2,
    lastName: 'toto';
    age: 34,
    },
    {
    id :3,
    lastName: 'toto';
    age: 34,
    },
];
app.delete('/api/users/:id',
    (req, res) => {
        const id = parseInt(req.params.id);
        users = users.filter((user) => user.id !== id);
        return res.send(users);
    });

```
Les tab dans Pm permetttent de créer des appéles.

Un API c'est un contrat pour utiliser les verbes et protocole et pour échanger des informations.

Dans postman on coche **DELETE** et on tape `http://localhost:3015/api/users/1` pour avoir la route. Le 1 correspond a l' id 
que l'ont veut supprimé.

`req.params` permet de récuperer l'info sur Pm.

L'id que l'on récupére apres un **send** sur Pm est un string. Pour avoir un number
il  faut ajouter `parseInt()` dans Ws.

On utilise filter() pour supprimé l'id 1

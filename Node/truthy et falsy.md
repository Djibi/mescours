# truthy et falsy

Falsy veut dire toute les valeurs qui sont fausse en js.On peut le verifier avec
un prédicat dans le console.log si il affiche 'ok' c'est true , sa existe si il affiche rien c'est falsy
```javascript
if(2, 3) {console.log('ok')}
ok
if(null) {console.log('ok')}
--------------------------
if('') {console.log('ok')}
------------------------------
if(undefined) {console.log('ok')}
--------------------------------
if(0) {console.log('ok')}
------------------------------
if(NaN) {console.log('ok')}
-------------------------------
if(-1) {console.log('ok')}
ok
if(1000) {console.log('ok')}
ok

if('coucou') {console.log('ok')}
ok

if('   ') {console.log('ok')} // l'espace est considérer comme un caractére
ok

if('!false') {console.log('ok')}// ! veut dire inverse
ok
if('!true') {console.log('ok')}
----------------------------------
if('!1') {console.log('ok')}//1 existe donc !1 veut dire l'invers de quelque chose qui   existe
--------------------------------
if('![2, 3]') {console.log('ok')}
------------------------------------
if('!![2,3]') {console.log('ok')}// !! permet de transformé tout chose en boolean
----------------------------
console.log(!! 0)
false
console.log(!! [])
true
console.log(!! '')
false




```  


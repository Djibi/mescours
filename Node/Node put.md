# Node put
Put sert a remplacer un objet par un autre a ne pas confondre avec patch.

Dans Postman on colle le localHost+l'id de l'objet a remplacer, puis coché body => raw => JSON.

Ensuite écrire le nouvel objet dans body
```typescript

app.put('/api/users/:id',
    (req, res) => {
        const newUser = req.body;
        const id = parsInt(req.params.id);
        const index = users
// ici on fait un if() pour verifier ce que l'on veut ajouter 
            .findIndex((user) => user.id === id);
// on peut aussi faire un map, une boucle for ou un forEach
        users[index] = {...user, id};
        return res.send(users);
    });

```
En generale dans le `return` on ne retourne que l'objet modifié car si il y a 1 millions d'utilisateur dans user sa retourne
le tout.  

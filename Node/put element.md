# put element

Put sert a remplacer l'intégarlité d'un objet par un autre.On vérifie qu'il n y a pas d'effet de bord.
![put.element](img/app.put.PNG)

![function-put](img/function-put.PNG)

`.set` est la même chose que `put`

![function-test](img/function-test.PNG)

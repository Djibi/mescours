# mise en place de node

Une fois le repo installer sur webstorm :

- Faire `cd server` puis 'npm i' dans la ligne de commande

- dans package.json faire `run npm install` en mettant la souris sur l'écran principale.

- taper `npm install -g concurrently` dans la commande (sa permet de jouer plusieur tache en même temps)

- package.json => show npn script => watch(a ne faire qu'une seul fois) => serve.

A partir de la on fait du typescript qui est équivalent au js   .

```javascript

import express from 'express';
const app = express();

app.get('/api', async (req, res) => {
    return res.send('coucou promo 3');
});
app.listen(3015, function () {
    console.log('API listening on http://localhost:3015/api/ !');
});

```
req: veut dire requete

send: veut dire repond

`app.listen` sert a définir le ports utiliser 

Dans cette exemple d'une page server.js, expresse est un serveur.
Donc app est une fonction qui fabrique une application serveur. 

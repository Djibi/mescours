# principes de firebase

Pour initialiser la base de donné dans le projet 
 
- dans le navigateur => firebase => acceder a la console => acceder a la documentation => Get started for admin(quand ont fait du node) 

cloud firestore => get started => on copie`npm install firebase-admin --save` ou `npm i firebase-admin -S` dans le terminal Ws (dans le dossier server)

le ` -- save` veut dire install dans notre projet et sa s'affiche dans package.json.

description des fichier dans package.json qui sont installé quand ont fait npm i:

```javascript

 "devDependencies": {
    "@types/cors": "^2.8.6",//permet de reconnaitre les infos de typage cors (gére les interfaces)
    "@types/express": "^4.17.2",//permet de reconnaitre les infos de typage express
    "@types/node": "^13.7.4",//permet de reconnaitre les infos de typage node
    "express": "^4.17.1",// gére les verbes(get, post ...)
    "pm2": "^4.2.3",// permet de relancer le serveur quand il plante
    "tslint": "^5.12.0",// pour verifier la qualité du code
    "typescript": "^3.2.2"
  },
  "dependencies": {
    "concurrently": "^5.1.0",
    "cors": "^2.8.5",
    "firebase-admin": "^8.9.2"
}
```
L' interet de package.json est que celui qui récupere le projet sur gitlab ou github et qui fait un npm i récupere tout
 les applications installé.

Pou se connecté il ya une manip obligatoire a faire :

Dans serveur.ts ajouté `import admin from 'firebase-admin' ;`

```typescript

admin.initializeApp({
  credential: admin.credential.applicationDefault()
});

const db = admin.firestore();

```
ce code veut dire qu'on initialise l'application firestore avec credential qui un systéme d'identification
  
ensuite avec `const db`(data base) ont crée la base de donnée 

ensuite ont fait 

```typescript

const ref = db.collection('hotel').doc('M8nwJWlkHiXb2BMkUhOP')

//ont crée une const pour allée chercher les collections avec un id générer
//automatiquement dans database sur firebase

```
**attention `const ref`  n'est pas l'hotel mais la référence qui permetde savoir ou est l'hotel comme une adresse postal**

pour avoir accés a toute la collection ont enléve .doc()

```typescript
app.get('/api/hotels/:id', async (req, res) => {
    try{  
        const hotel = await ref.get();    
        return res.send(hotel);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});

```
ici ont remplace `res.send`(hotels)par les infos qui sont dans la base de donné
sur firebase donc la référence qui pointe vers `db collection` , c'est le serveur qui récupere les données sur firebase
puis les transmets, il y a deux étapes donc on met le mots `async` devant req

`async`: quand on va chercher des infos dans la base de donné
 
`await`: veut dire attent le résultat

`ref.get`: c'est une promesse qu' on va avoir une réponse

si le serveur bug c'est qu'il n'a pas trouvé 'credential' donc il faut les lui fournire
pour activer la connection avec firebase. 

## generate private key
 
il faut générer les crédential (clé de service) pour être reconnue par firebase comme application légitime et être
autoriser a accedé a ses donnée.

- Dans firebase => le projet

- roue dentée en haut a gauche => parametre du projet

- compte de service => SDK admin firebase => générer une nouvelle clé privée

**IMPORTANT**

**Si on met la clé dans repot un gitlab ou github publique tout le monde a accés a la clé et peut la copié
pour faire des modif dans votre base de donnée.**
 
**Donc faire attention si on active la version payante de firebase qui est facturé au nombre d'appel.** 

une fois le téléchargement fini :
 
- dans Ws crée un dossier `cred` dans server

- collé la clé dans cred et faire un nouveau fichier `cert.ts`

- dans cert tapé `export const cert = {}` et copié-collé la clé a l'intérieure

- ensuite enlevé les ' ' comme ci-dessous

![cert1](img/cert1.PNG)

![cert2](img/cert2.PNG)

C'est cette clé qui permet de se connecté a la base de donné.

## activate cerf and get one doc

La derniere étape est de lié la clé au `admin.initializeApp` dans `serveur.js

Dans serveur.js remplacé le code ci-dessous
```typescript

Vadmin.initializeApp({
   credential: admin.credential.applicationDefault()
 });
 
 const db = admin.firestore();

```

par celui-ci pris sur firebase => settings

![initializeApp](img/initializeApp.png)

remplacé les " " par ' ' pour databaseURL

`cert(serviceAccount)`: on doit y mettre le certificat qu'on a crée

En faisant `ctrl` + double clique sur `cert` sa affiche:

![function cert](img/function%20cert.PNG)

Dans cette function on a deux parametre soit un string(mais on en a plusieur ds le fichier cert)soit
un objet de type serviceAccount.

on obtient ceci en cliquant sur le 2éme serviceaccount et on se sert de sa pour fair les modif de cert.

![service-account](img/service-account.PNG)

on obtient ceci apres modification du cert

![import-serviceaccount](img/import-serviceaccount.PNG)

remplacer (serviceAccount) par (cert) et fair l'import:

![modif-cert](img/modif-cert.PNG)

on essaye sur un hotel en ajoutant .doc() avec l'id que l'on récupérent dans la base de donnée. 

![const-ref](img/const-ref.PNG)

![id](img/id.PNG)

on modifie hotel pour le mettre au singulier dans app.get car on ne cherche qu'un hotel

on ajoute `.data()` qui permet d'avoir les infos de l'hotel

![data](img/data.PNG)

Au final on récupérent la même chose que dans la base de donnée

![get-final](img/get-final.PNG)

![bdd](img/bdd.PNG)

# get collection

On va voir comment faire un get sur toute une collection:

![function.get-collection](img/function.get-collection.PNG)

![get-collection](img/get-collection.PNG)

on récupére toute la collection sur postman:

![get-postman](img/get-postman.PNG)

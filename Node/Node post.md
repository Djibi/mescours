# Node post

Post sert ajouter un nouvelle élement
```typescript

let users = [
     {
    id: 1,
    lastName: 'toto';
    age: 34,
    }, 
    {
    id : 2,
    lastName: 'toto';
    age: 34,
    },
    {
    id :3,
    lastName: 'toto';
    age: 34,
    },
];

```
```typescript

app.post('/api/users',
    (req, res) => {
    const newuser = req.body;
    const id = users.length + 1;
    users.push({...newuser, id});
    return res.send(users);
    });

```

Dans Postman on peut ajouté des objet JS :

dans Pm => body => raw(brut) => Text => JSON(format d'échange de donné js).
  
En JSON on met des "" et pas de , pour les number

Quand on veut utiliser `req.body` il faut faire une modif on utilser le bodyParser,
il faut ajouter `import bodyParser from 'body-parser';` et `app.use(bodyParser({}))`dans la page **server.js** 
 
cela permet de lire ce qu'il y a dans le body sur Pm.

Ensuite on crée un nouvel id avec `const id = users.length + 1;`

puis on ajoute l'id en déstructurant l'objet avec `({...newuser, id});`

 


# delete element


On delete un élément grace a l'id que l'on prend dans la base de donnée.

Un effet de bord: c'est quand on modifie une chose et que cela a des répercutions sur d'autres choses.

Donc on doit vérifier ou est utiliser la fonction avec `ctrl + double clique sur le nom de fonction`

![function-delete](img/function-delete.PNG)

`refHotel.doc()` fait référence a la liste des hotels dans la base de donnée

quand ont fait un `get` il peut arriver que l'hotel n'existe pas donc on fait un catse c'est a dire on force typescripte
a accepté le typage HotelModel avec `as`.

`await`: on attent la suppression de l'id et on met le resultat dans `deletResult`

`.delete()`: pour la suppression 

![app.delete](img/app.delete.PNG)

![id-delete](img/id-delete.PNG)

on fait le delete sur postman avec l'id a supprimer on obtient l'image ci dessous qui est le temps de réaction

![writResult](img/writResult.PNG)

pour verifier l'id supprimer on utilise un template literal:

![retourId](img/retourId.PNG)

![resultatId](img/resultatId.PNG)



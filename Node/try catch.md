# try catch

try : essayer(chemin normale)

catch : attraper(en cas d'erreur)

On attrape des erreurs. Les serveures qui contienne des applications avec du codes mal écrit crache des erreures.A chaque
fois que le serveure crache il faut le redémarrer.Pour eviter qu' un serveure plante on utilise un try cache qui permet d'eviter
que le systeme ne plante.

Pour tout les routes que l'on fait on utilse un try-catch:

### sur la page serveur.js

 ```typescript

app.delete('/api/hostels/:hostelId/rooms/:roomId', (req, res)=> {
    try {
        return manageRoomDelete(req, res, hostels);
    } 
    catch (e) {
        return res.status(500).send(e.message);
    }    
});

```

`.status(500)` veut dire que le serveur a planter
   
### sur la page hotel.service.js
 
```javascript

export function manageRoomDelete(req: any, res:any, hostels:HotelModel[]) {
    const hostelId = parseInt(req.params.hostel.Id);
    const roomId = parseInt(req.params.roomId);
    const hostelToFind = getHostelById(hostels, hostelId);
    if(!hostelToFind) {
    throw new Error('room not found');    
    }
    const indexOfRoomToDelet = getRoomIndexById(hostelsToFind.rooms,roomId)
    if(!indexOfRoomToDelete) {
    throw new Error('room not found')    
    }
    hostels.splice(indexOfRoomToDelete, 1);
    return res.send({message: 'delete ok'});
}
    
```

`throw` veut dire renvoie 


# Node patch 

Patch sert a modifier un attribut dans un objet. patch veut dire mettre a jours.

Dans postman :
- on copie-colle le localHoste+l'id a modifier
- on écrit l'attribut a modifier dans le body
```typescript

app.patch('/api/users/:id',
    (req, res) => {
        const newUser = req.body;
// ici on fait un if() pour verifier ce que les data que l'on veut ajouter
//correspond a la base de donné  
        const id = parsInt(req.params.id);
        const index = users
            .findIndex((user) => user.id === id);
// on peut aussi faire un map, une boucle for ou un forEach
        users[index] = {...users[index], ...newuser};
//la ligne au dessus veut dire qu'on garde l'ancien index et ajoute newuser sinon sa ne garde que l'attribut modifié 
        return res.send(users);
    });

```



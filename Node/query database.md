# query database

on cherche tout les rooms qui ont une roomNumbers = 15
![function-where](img/function-where.PNG)

![get-where](img/get-where.PNG)

On met parseInt pour avoir des nombres et non des string

![where-result](img/where-result.PNG)



`.where`: veut dire ou. On peut mettre: ==, <, >, <=, => et on récupére des document data donc
on rajoute `.get` et `await` 

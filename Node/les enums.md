# les enums

Quand une room ou autre chose a plusieurs états, on utilise `enum ` qui est une interface qui permet de décrire un état dans lequel

est un objet.On peut écrire en francais entre les ' '

```typescript

export interface RoomModel {
    id: number;
    size: number;
    roomName: string;
    roomState: RoomState;

}

export enum RoomState {
    isAvailable = 'est libre',
    isCleaning = 'en nettoyage',
    isOccupied = 'occupée',
    isClosed = 'en travaux',
    sjksjksjk = 'tout moche',
}

```

## set and get enum state

On va voir comment changer l'etat d'une room et 'geter' l'etat d'une room

on fait une fonction state pour vérifier que les info que l'ont nous donnes correspondent aux info en base de donnée.

![function-state](img/function-state.PNG)

![functionroom-state](img/functionroom-state.PNG)

![patchstate](img/patchstate.PNG)
* Dans le fichier rooms.service.ts, on écrit :

```typescript
export async function getRoomState(roomId: string): Promise<RoomState> {
    const room: RoomModel = await getRoomById(roomId);
    return room.roomState;
}
```

Cela signifie :

Récupère une room par son id grâce à la fonction getRoomById, et renvoie-moi l'état de la room

* Dans server.ts, on a plus qu'à faire un get :

```typescript
app.get('/api/rooms/:roomId', async (req, res) => {
    try {
        const roomId: string = req.params.roomId;
        const state: RoomState = await getRoomState(roomId);
        return res.send({state});
    } catch (e){
        return res.status(500).send({error: 'erreur serveur' + e.message});
    }
});
```

On obtient bien l'état sous forme d'objet :

![op](img/op.PNG)




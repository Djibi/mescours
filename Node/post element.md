# post element

Pour pouvoir faire des modif dans une base de donnée on utilise des outils dans Ws et non direct

sur firebase car il y a un risque de faire des mauvaises manipulations.

on peut faire des modifications au niveau des function sans toucher au reste

dans serveur.ts

![post](img/post.PNG)


dans hotels.service.ts

![functionpost](img/functionpost.PNG)

on délégue la gestion de la base de donnée a des fonctions sans avoir a toucher au fichier principale

pour cela il faut accedé a `initializeApp` `const db` et `const ref`

on peut mettre des fonctions en commentaire avec /*  */

on prend db et ref et admi qui sont dans serveur et ont les trnsféres dans l'endroit ou l'ont fait les modifs dans notre exemple hotels.service.ts

```typescript

const db = admin.firestore();

const ref = db.collection('hotels');

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: 'https://wr-promo3-node-fe11f.firebaseio.com'
});

``` 

ajouté un point a cert si besoin

![cert](img/cert.PNG)

il import `import admin from 'firebase-admin';` automatiquement

on modifie const ref :

`const refHotels = db.collection('hotels');`

le resultat final de la fonction pour post

![function-newHotel](img/function-newHotel.PNG)

`add`: ajoute un document et un id a une collection par contre il renvoi vers un document:

`async` : quand on fait une modif dans une base de donnée extérieur on passe par internet donc on envoi une donnée et on attent la réponse  

`await`: on a `async` donc on met `await` pour dire qu'on attend qu'il écrive en bdd puis on renvoi le résultat

donc on rajoute aussi `await et async` dans post sur serveur.ts

![newpost](img/newpost.PNG)

ajouter l'objet dans postman en supprimant l'id car il va etre générer par firebase

![postpostman](img/postpostman.PNG)

en cliquant sur send on obtient le fichier document générer par `add`

![send](img/send.PNG)

`return {...newHostel, id: addResult.id);` le nouvel hotel + l'id crée par la base de donnée

![room](img/room.PNG)

on rajoute 'string', le ? veut dire undifined

![roommodifié](img/roommodifié.PNG)


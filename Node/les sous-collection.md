# les sous-collection

![sous-collection](img/sous-collection.PNG)

On crée d'abord les collections hotels et rooms séparement.

On crée ensuite une sous-collection room dans hotels

![interieure.sous-collection](img/interieure.sous-collection.PNG)

dans la sous-collection rooms on met juste l'uid et la ref qui est un lien vers la rooms.

![infossous-collection](img/infosous-collection.PNG)

les infos sont mis dans la rooms ex: name,pools,size et non dans la sous-collection dans l'hotel.

## ajouter un element dans une sous-collection

![ajout-element](img/ajout-element.PNG)

on fait une fonction ou l'ont verifie que l'hotel existe

on ajoute la chambre dans la nouvel table `.add`

on récuperent le résultat de cette ajout dans `creatRoomRef`

et on modifie l'hotel en ajoutant dans la sous-collection rooms les nouvelles infos


![function-post-element](img/function-post-element.PNG)

![post-element](img/post-element.PNG)

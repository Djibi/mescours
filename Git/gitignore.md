# gitignore

Quand on gére des repo il y a des dossier ou fichier qu' on ne veut pas renvoyer sur git comme nodes_modules sa surchage les projets et ce n'est pas bon pour l'écologie.

Pour les enlevés :

- Créer un fichier .gitignore a la racine.

- ctrl+R pour y mettre tout les fichiers inutiles

Le dossier .gitignore est la premiere chose a faire sur un projet avant de créer d'autre dossier; si les dossiers sont créer
 
 avant le .gitignore il faut les enlevées grace au terminal:
 
 - faire `git rm -rf .idea`  ou ` git rm -rf nodes_modules`
 
 Si webstorm se bloc; fermé et réouvrir puis si besoin faire un `npm init` pour réinstaller nodes_module
 
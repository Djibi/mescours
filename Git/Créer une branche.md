# Créer une branche

La master: c'est le projet initial accepter par le chef de projet.

A plusieur on ne travail pas sur la master on crée une branche:

- Dans webstorm en bas cliquez master > new branch > mettre un nom.

- Une fois le travail fini sur la branche on fait commit and push

Il est interdit de faire checkout avant d'avoir fait commit.
 

## Envoyer a la master

- Sur gitlab > Creat merge request > submit merge request.

- Une fois les modif acceptés sur la master fair une merge (fléche bleu) pou récupérer la derniere version de la master.


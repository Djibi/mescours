# output et event emitter

On va voir comment remonté une info au 'parent', pour ca on a besoin d'un **event emitter** 
 
On l'appel `handleMoodChange`

![event](img/event.PNG)

on l'initialise avec `new EventEmitter`

**un event emitter est une fonction qui est fournis par angular et qui permet a un component
child d'informé un component parent de ses modif.** Cette fonction émet une info quand il y a
un changement.

![event2](img/event2.PNG)

a chaque fois que l'on clique sur l'un des boutons sa va envoyer
un message a tout les endroits qui sont liée au `andleMoodChange` 

Maintenant on va liée le parent `app.component.ts` au event emitter

![event3](img/event3.PNG)

Quand c'est un Input on utilise des crochets et des parenthéses pour les Output.

Quand il y a un **emit** on le recois dans **app.component.html** au niveau de <app-mood>
et on traite cette info grace a une fonction `manageMoodChange`, elle fonctionne comme une fonction de callback.

![event4](img/event4.PNG)

`manageMoodChange` prend en paramtere message qui est celui qui est dans `emit`;

Dans `emit` on aurait pu mettre un number,un boolean, ou autre chose.

On met une interpolation `{{message}}` dans < h1 > pour avoir un message qui s'affiche dans le navigateur

`($event)` est la message de `emit`


# interpolation simple

Le `< router-outlet >` permet d'avoir la page ou l'on navige

Dans notre exemple on veut avoir écrit: 

![name](img/name.PNG)

Pour ca ont utilise l'interpolation du html, cela veut dire que html peut calculé des choses qui viennent du component et cela se fait avec des doubles crochet `{{ }}`

 il faut pour ca créer une variable dans la class `Menu1Component` dans le fichier component.ts

![name3](img/name3.PNG)

![name2](img/name2.PNG)

![init](img/init.PNG)

le typage de name n'est pas nécéssaire car on ne peut avoir qu'un string mais si on veut absolument le mettre il faut faire une modif:

`ngOnInit`:c'est la fonction qui va se déclenché a l'initialisation du component l'équivalent du `render` ou du `return` en react
 
 comme on est dans un une classe on doit mettre `this` pour accéder a la propriété name

![total](img/total.PNG)

**Donc pour faire l'interpolation on déclare des variables de facons implicite ou explicite**

## interpolation en détail

En react ont peut faire du code dans une interpolation mais pas en angular

En angular on peut avoir une opération

![op](img/op.PNG)

On peut créer une fonction

![op3](img/op3.PNG)

![op2](img/op2.PNG)

**Dans une interpolation en angular il n'y a pas de logique mais juste le résultat d'une opération donc pas de if, map, boucle, array**

sa marche avec des strings

![op4](img/op4.PNG)

il ne lis pas les objets sauf en faisent **| json**

![op5](img/op5.PNG)

![op7](img/op7.PNG)

![op6](img/op6.PNG)

on peut aussi faire comme ceci même si ce n'est pas la meilleure facons de faire

![op8](img/op8.PNG)

## interpolation attribut

![link](img/link.PNG)

`routerLink` pointe vers le string "home"

`routerLink` est une propriétés comme en html mais qui est gérer par angular. Comme il n'y a pas de `{{}}` sur router  

angular comprend que c'est un string. **On va voir comment mettre une variable a la place du string pour routerLink**

![top](img/top.PNG)

On met des [ ] a `routerLink` pour lui expliquer qu'il doit aller chercher la valeur qu'il y a dans link

![top2](img/top2.PNG)

si on fait un `ctrl + clique` sur les link cela nous envoi vers les link des autres pages donc les liens sont bon. 

**Donc une interplollation d'attribut se fait en mettant des [ ] au noms de l'attribut**

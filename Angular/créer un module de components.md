# créer un module de components

![lien2](img/lien2.PNG)

On veut faire des deux lien un module dans lequel il y a un component pour l'utiliser ou l'on veut dans l'application:

Un module ou il n'y a que des components n'est pas liée a des routes.En angular on met des - et non des majuscules

![lien3](img/lien3.PNG)

ce sont des modules ou l'ont va mettre plusieurs component  pour pouvoir les importés a partir du module

![lien4](img/lien4.PNG)

![lien5](img/lien5.PNG)

![lien](img/lien6.PNG)

![lien7](img/lien7.PNG)

![lien8](img/lien8.PNG)

le fichier `main-menu.module.ts` est mis a jour automatiquement

la `declarations` sert a déclaré des component dans un module

`export` on y met les components que l'on veut laisser en accès libre a l'exterieure du module.

`export` est intéressent quand on a un component qui est commun a plusieur component exemple un logo et que l'on ne veut pas le laisser seul en accès libre

![logo](img/logo.PNG)

![logo1](img/logo1.PNG)

![logo2](img/logo2.PNG)

**Le component doit avoir le mêmes nom que le selector**

En angular on met un préfixe avant le nom c'est une convention ex: `< app-menu-logo>`

donc  `< app-menu-logo>` est un component dont on ne peut se servir que dans menu1 et menu2

Le routeur est un module qui nous vient d'angular donc pour pouvoir l'utiliser dans menu1 et menu2 **il faut d'abord l'importé dans main-menu.module**

![lien9](img/lien9.PNG)

en faisent `Alt`+`Entrer` sa import le routerModule dans `main-menu.module`

On va utiliser les menu1 et 2 dans la page principal 

**il faut faire l'import de MainMenuModule dans app-module.ts pour pouvoir l'utiliser dans la page html**

Donc `MainMenuModule` que j'ai fabriquer je le rend disponnible dans d'autre module

![menu](img/menu.PNG)

![menu](img/menu2.PNG)

![menu3](img/menu3.PNG)

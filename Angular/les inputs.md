# les inputs
Dans les exemples précedent on a mis des objets a l'intérieure d'un component `allRoute` et il ne sont utilisables que par lui,

![allRoute2](img/allRoute2.PNG) 

On va remonté l'infos d'un crant pour qu'elle puisse être utiliser autre part

![allroute3](img/allRoute3.PNG)

on met `alllRoute` dans **app.component.ts**.

**app.component.ts** utilise ` <app-menu1> ` pour qu'il puisse lui passer en props l'objet dont il a besoin. **Pour cela on va aller déclarer
dans `app-menu1` qu'il peut prendre un input , on l'appel aussi un décorateur.**

![input](img/input.PNG)

![route3](img/route3.PNG)

dans `menu1.component.html` on remplace `AllRoutes` par `routes`

Ensuite on injecte **l'input** dans **Menu1Component** grace a ` < app-menu1 > `

![route5](img/route4.PNG)

On veut aussi que l'age ne soit plus dans le component mais viennent du pére

![fils](img/fils.PNG)

**En faite les infos ne sont plus dans menu1.component mais dans app.component et on les import dans menu1 grace au input.**

On fait une varible age et on passe la variable **age** a `< app-menu1 >`

![age](img/age.PNG)

![age2](img/age2.PNG)

![age3](img/age3.PNG)

![total2](img/total2.PNG)

On appel cela de la délégation de compétence

**le travail de app.component.ts et de passer des variables et de les donnés a ceux qui en ont besoin**

**menu1.component.html sait ce qu'il doit affiché grace au `ngIf` `ngFor` `ng-container`**

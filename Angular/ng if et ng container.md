# ng if et ng container

On va voir comment utilisé la directive `ng if` dans le html.Dans notre exemple on veut que 'info pour les vieux' ne soit disponible
que pour les gens qui ont plus de 32ans

![vieux](img/vieux.PNG)

![vieux2](img/vieux2.PNG)

![vieux3](img/vieux3.PNG)

Maintenant on veut que la page `à propos` ne soit disponible que pour les plus de 32ans

![jeune](img/jeune.PNG)

![jeune3](img/jeune3.PNG)

![jeune2](img/jeune2.PNG)

On veut faire une modif pour ne plus avoir le point

![attention](img/attention.PNG)

**on ne peut mettre qu'une seul directive fonctionnel par balise**

Quand il y a une étoile on appel sa une directive structurel, donc en angular on utilise `ng-container`

![container](img/container.PNG)

2émé solution mais la 1er est a privilégier

![container2](img/container2.PNG)

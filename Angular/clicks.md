# click

Dans les leçons précedents on a vu comment un parent donne des infos a un 'child' grace au `@Input`

On a vu comment stocker des infos commune a plusieur component dans un même service.

**Maintenant on veut que le component 'child' qui subit une modif transmette cette info au component 'parent'.**

Pour ca on crée un component avec 3 bouton 'd'humeur' et l'on veut que quand on clique sur un des boutons l'info remonte

a `app.component.html`donc on pourra le modifier autant que l'on veut, l'info qui remonte elle ne changera pas.

On crée un component qu'on appel 'mood' qui veut dire état d'esprit

![mood](img/mood.PNG)

`m`  on crée un module ou l'on va peut être plusieur component

On le met dans le dossier **component** car on risque de l'utiliser a plusieure endroit

comme on aura qu'un seul component on ne fait qu'un seul mood.component qui sera ratacher au mood.module

![mood2](img/mood2.PNG)

![mood4](img/mood4.PNG)

On fait l'export de `MoodComponent` pour pouvoir l'utiliser ailleur

Pour s'en servir dans `app.component`:

![mood5](img/mood5.PNG)

![ok2](img/ok2.PNG)

Quand on a un verbe avec des `( )` comme ci-dessus c'est quand on a verbe d'action c'est a dire quand on va cliqué cela va déclencher un **outpout, une sortie d'info

On fait une fonction pour chaque 'clique'

![ok3](img/ok3.PNG)


![ok4](img/ok4.PNG)
**Quand on veut déclencher une action on met ( ) autour du verbe et on rajoute une fonction**

On a vu comment faire le click on va voir comment remonté l'info au component 'parent' dans le cour **outpul et event emitter**
 

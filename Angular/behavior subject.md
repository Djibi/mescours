# behavior subject

On a vu comment un 'parent' pouvait réagire en fonction de ce qui ce passer chez le 'child' mais les deux était en relation direct

mais on peut aussi faire réagir d'autre 'parent' qui ne sont pas en lien direct avec le child.Dans notre exemple on n'imagine

que `app.component` n'a pas de lien avec `mood`.Pour faire cela on crée un nouveau component `moodService`

![service](img/service.PNG)

Dans notre systéme on veut pouvoir avertir les gens quand il se passent quelque chose.En angular il n'y a pas de Domevirtuel

mais un systéme équivalent avec des events.**On va crée un émetteur d'évenement qu'on appel un 'behavior subject' = sujet de comportement**

On observent une variable et quand elle change on trigger un event et c'est ce qui va faire reagir le parent.

**Quand on a un behavior Subject ou un autre trigger on met $ a la fin sauf pour eventEmitter c'est une norme angular.**

On l'initialisent avec new et un string vide ou 'null' 

On a une fonction `setMood` qui va permettre de changer le mood dans lequel est la personne

`next` veut dire la prochaine valeur que l'on va envoyer.

![child](img/child.PNG)

On va pouvoir se lien a `handleMood` et la premiére fois on va avoir une valeur null et dès que `setMood` va être appeler
on va avoir le message.

![all](img/all.PNG)

Quand `app.component` s'initialise on va s'abonner au service qui nous envoi le moodService

On initialise un `app.component` dans le `ngOnInit` pour utilisé la fonction on rajoute `implements OnInit` 

![fin2](img/fin2.PNG)

`subscribe` veut dire qu'on s'abonne a `moodService` pour récupérer le message

On fait une étape intermediaire avec le `pipe` pour traiter les infos qui nous viennent de `handleMood`

un `tap` est une opération dans le pipe qui sert a faire une petit action

![fin](img/fin.PNG)
 
  

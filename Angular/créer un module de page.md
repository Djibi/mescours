# créer un module de page

Dans Google tapé : angular cli => get started, on arrive sur la doc oficielle d'angular.

On va découvrire les **ng generate**, ca sert a fabriqué des class, component, directive...

![generate](img/generate.PNG)

On va voir comment on gérent un module.Angular est organisé en module, on découpe l'application en module et chaque module va avoir un but

**Il y a deux types de modules**: 

- les modules de **types pages** ou il y aura l'integralité d'une page.Si on a une page qui s'appel home on doit avoir un module
qui s'appel home.Dans ce module on y met **tout le html et css**.

- les modules de **types component** ce sont des module dans lequel on va mettre des component qu'on va pouvoir réutilisé partout.

Le module de type page est composé de plusieur component réutilisable ex: un module < header >, un module < footer > et

on va importé ces modules dans les modules principaux.

Dans le terminal tapé :

`ng g m home --route=home --module=app-routing`

`g` pour generate, `m` pour module, `home` car on fabrique une page home,`--route` veut dire on créer une option quand on va tapé `/home`

on va arriver sur la page home, `module` pour le ratachement du module dans notre cas a `app-routing`

Quand un module a une route c'est qu'il est de **type page** et on accroche cette page dans `app-routing`
 
On appui sur entrer et la page home est créer:

![home](img/home.PNG)

`home.module.ts` est le module qui va contenir le module de la page

On a la le routing principale de l'application
![route2](img/route2.PNG)

C'est ou on va avoir une const routes ou l'on va stocker l'architecture des pages

`path`: le chemin

`loadChildren`: charge le module

`.then`: une promesse comme await , async

Ensuit on crée une seconde page:

![about](img/about.PNG)

Une route est crée automatiquement dans `app-routing.module.ts`

![prop](img/propo.PNG)

Dans le fichier `app.component.html` on supprime tout et on y met :

![app](img/app.PNG)

le router-outlet c'est le routeur principale ou l'ont rattache les infos des routes

![app2](img/app2.PNG)

## le module de home

![module](img/modul.PNG)

ce qui est en bleu est un décorateur qu' on ajoute a la classe HomeModule

et c'est lui qui va expliqué a angular commment fonctionnent la classe HomeModule

Le décorateur importe d'autre module

`CommonModule` qui est module obligatoire ou il y a les infos de base de angular

`declaration` veut dire qu'est-ce qu'il y a dans le module

**en angular il y a des modules dans lequel il y a des component**

![home2](img/home2.PNG)

dans ce fichier il y a un décorateur dans lequel on a

`selector` pour se servir du component dans du html

`templateUrl` pour dire ou est le texte

![home](img/home3.PNG)

![home](img/home4.PNG)

![home5](img/home5.PNG)

 si on mettre la classe 'maine'**dans app a la racine** sa ne marche pas car le **css n'est gérer que par component**

![home6](img/home6.PNG)

## créer un lien

On crée des lien pour se déplacer entre la page `home` et `about`    

En angular quand on fait un lien on utilise `routerLink` et non `href` car cela reload la page et toute l'appli angular a chaque fois.

![lien](img/lien.PNG)

![lien](img/lien2.PNG)

# post et delete

Pour crée un nouvel hotel:

Dans notre back on fait un post sur la liste d'hotel donc dans **post** on rajoute **'hostels'**.

Pour le post ce qu'il retourne dépend du back-end:

Le post du back nous renvoie un `addResult` qui lui est égale a `postNewHostel`
et postNewHostel nous donne une promesse d'<hotelModel> donc c'est bon.

![hostel](img/hostel.PNG)

![post](img/post.PNG)

ensuite on crée un bouton

![click](img/click.PNG)

![post3](img/post3.PNG)

On a pas besoin de faire unsbscribe car avec un post on ne s'abonne qu'une seul fois

![test](img/test.PNG)

![post4](img/post4.PNG)

On peut le verifier autrement:

![tap](img/tap.PNG)

![tap2](img/tap2.PNG)

En ajoutant le **pipe** on peut le voir direct dans la console.

![delete](img/delete.PNG)

![delete2](img/delete2.PNG)

![Database](img/Database.PNG)

On va chercher dans firebase l'id de l'hotel que l'on veut supprimer.
 
![delete3](img/delete3.PNG)

Dans l'exemple il y un bug dans le back-end le writResut renvoie string alors que angular veut qu' on lui renvoie un objet, on fait la modif:
![bug2](img/bug2.PNG)

![bug3](img/bug3.PNG)

![bug](img/bug.PNG)

![delete final](img/delete%20final.PNG)

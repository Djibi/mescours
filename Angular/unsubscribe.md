# unsubscribe

On va voir comment on gérent le désabonnement.Si on ne se désabonnent pas a chaque fois que l'on quitte la page et que l'on revient 
cela relance le `ngOnInit` doc a force il y a un risque de 'memory lik' fuite de memoire ou des 'stack over flow' remplissage
de la mémoire trop important.

Pour cela on utilise le `Destroy`, quand le component n'a plus besion d'être la il s'auto détruit  
 et fait le désabonnenment a ce moment la, donc on ajoute `ngOnDestroy`

![on](img/on.PNG)

1er méthode:

je crée une variable `this.subsiscrition`dans `ngOnInit` donc mon abonnement s'enregistre dans cette variable.

Et quand on est dans `ngOnDestroy` on va se désabonné grace a `unsubscribe`

![on2](img/on2.PNG)

Ce n'est pas la meilleure méthode car cela devient difficile quand il y plusieur subscription

2émé méthode:'teaken teal' sa veut dire reste abonné tant que...

![on3](img/on3.PNG)

**c'est un Subject et non un behavior subject la différence c'est qu'il ne s'initialise pas on l'appel aussi observable**

![on4](img/on4.PNG)

cette observable quand il arrive dans destroy on met`this.destroy$.next` pour émettre une value.

On a dit que le **subject** était un **boolean** donc next sera tru ou false mais on aurait pu mettre autre chose.
 
Ensuite on ferme le `destroy` grace au complete pour pas que lui aussi fasse des 'stack over flow'

Donc dans le pipe on va s'abonner tant que le destroy n'a pas emit la valeur true grace au `takeUntil`

**c'est pour cela qu'on utilise un subject et non un behavior subject car en faisant la premiére émission a 'null' sa déclencherai
 le `takeUntil` et l'app s'arreterait de suite.**

# varaible d'env

Avant de faire les appels au serveur on va voir les variables d'environnements.

Quand on code en local on va avoir une adresse api en localhost...

Mais dans la réalité on a une adresse en www...

![apil](img/apil.PNG)

Dans le projet angular dans le dossier environments,il y a deux fichier qui se ressemblent avec un objet dedans

En local sur notre ordinateur on utilise `environment`

Et l'autre quand il sera mis en prod

![api2](img/api2.PNG)

![api3](img/api3.PNG)

On met la même chose dans `environment.prod` mais par la suite on mettra un adrress en www...

Maintenant le fichier environment est accéssible en injection dépendance n'importe ou dans l'application et on a accès a l'api.

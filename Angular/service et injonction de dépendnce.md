# service et injonction de dépendance

![papa](img/papa.PNG)

Dans le cours précédent on a vu comment un component parent passe des données a un component enfant mais **ce n'est pas la place de données d'être
dans un component.Il sont la pour gérer les actions de l'utilisateur.**

**En angular les données vont dans un service**

Pour créer un service tapé dans le terminal:

![menu.g](img/menu.g.PNG)

`g` pour generate 
`s` pour service

On va crée un dossier commun a tout les services on l'appel menu par défaut

Le serice est un **Injectable** c'est un décorateur ou l'on va pouvoir stocker des infos dans le service pour pouvoir les importés
la ou l'ont en a besoin.

On récupérent `allRoutes` et on le met dans `menu.service.ts`

**Dans une class on met d'abord les attributs, puis le constructor et aprés on met les méthodes**

Ensuite il faut dire a `app.component.ts` qu'il doit aller chercher un service et pour injecté un service on crée le constructor

Ce service ne va être utilisé que dans `app.conponent` c'est pour cela que l'ont met `private` on lui donne le nom que l'on veut.

![private](img/private.PNG)

il est de type `MenuService` on va l'importé en faisant `Alt`+`Enter` et `shift`+`F6`+`Fn` sur le nom pour le renommé

![private2](img/private2.PNG)
 
Le menu service de droite est pour expliquer que l'ont va injecter le service et avoir accès a ce qu'il a dedans c'est a dire `allRoutes`

celui de gauche pour rappelé le nom du service

![change](img/change.PNG)

Si ont a besoin d'accèder a `allRoutes` dans 50 endroit dans notre application cette maniére est plus simple car il n'y aura qu'un seul changement a faire,

ou si `allRoute` est 5 étages a l'intérieure du component principal il faudra tout parcourire alors qu'avec cette maniére

on y accède directement.**Donc le service sert a accèder a des infos sans passer par des inputs**

![face](img/face.PNG)

![face2](img/face2.PNG)

On efface `@Input`pour route car il passe par le service. On efface `[route]` dans `app.component.html`

![total3](img/total3.PNG)

pour que `menu1.component.ts` accède au service on fait le `constructor` 

on remplie le MenuModel de routes qui est vide par le `this.routes` qui est dans `ngOnInit`

**Donc quand c'est simple ont passe des infos via un Input et quand c'est multiple, long ou compliqué ont passe les infos 
via un service**

# switchmap

![met](img/met.PNG)

On va voir comment faire pour récuperer les uid de room qui se trouve dans un tableaux.

![boutonget](img/boutonget.PNG)
 
 On commence par créer deux bouton get.Avec les deux fonctions.

![fn](img/fn.PNG)

Quand on clique sur les boutons on récupere les bonnes info dans la console.

![ts](img/ts.PNG)


Maintenant on veut aller chercher l'hotel et les infos des 3 rooms.Pour sa on va faire un appel supplementaire

avec un pipe.On est sur le tuyau de l'hotel et por commencer on va passer sur le tuyau d'une seul chambre,on va 'switchmapé'

![room](img/room.PNG)

On récupere l'hotel et les infos d'une room.

![room2](img/room2.PNG)

Avec le map on va chercher les id de chaque room

On recupére ce qui est dans le **combineLatest** dans le **tap** de fin.

![combine](img/combine.PNG)

Quand on appui sur **get hotel** on a l'hotel et les 3 chambres

![rooms](img/rooms.PNG)

Maintenant je veux avoir un objet hotel dans le navigateur

![appp](img/appp.PNG)

![p](img/p.PNG)

![pp](img/pp.PNG)

![ppp](img/ppp.PNG)

![pppp](img/pppp.PNG)

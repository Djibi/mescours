# form validation

Ce qui est important avec les formulaire c'est si il respecte bien les champs et les formes ou pas.On appel ca les validateurs 

![req](img/req.PNG)

`required` veut dire n'est pas valide si le champs est vide

![req2](img/req2.PNG)

![req](img/req3.PNG)

![max](img/max.PNG)

`maxLength` pour la longueur max

Pour avoir une case  de validation a coché:`requiredTrue`

![true](img/true.PNG)

![champs](img/champs.PNG)

en cas de forme invalide:
![send2](img/send2.PNG)

![inv](img/inv.PNG)

Il y a des moyens de voir d'ou vient une erreur

![div](img/div.PNG)

![maxl](img/maxl.PNG)



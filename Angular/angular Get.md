# GET

En angular quand on veut parler a une api il faut faire un service.Il est interdit d'appelé une api directement d' un component


On crée un service hotel depuis le terminal:

![hotel](img/hotel.PNG)

On veut aller faire des requettes 'http'

grace a cela on peut aller rechercher des ressources sur internet

![client](img/client.PNG)

Ensuite on crée une route, c'est la racine de l'api.On choisi `environment` elle sera mis automatiquement dans `environment.prod`

Quand on la changera dans l'app sa la changera automatiquement partout ou on s'en est servit.

![rooti](img/rooti.PNG)

En angular on met un $ a getHotelById, quand on fait un appel http client c'est un observable et c'est une fonction

![as](img/as.PNG)

Comme on est en typescript on doit typer les fonctions pour cela on va copier-coller **hotel.model.ts et room.model.ts** 

du projet Node

![node](img/node.PNG) 


![node2](img/node2.PNG)

![clicli](img/clicli.PNG)

la fonction nous renvoie un observable de hotelModel[] on rajoute **as**pour forcé l'hotelModel

Donc on va avoir 2 observable, un qui observer des hotels et l'autre va observer des array d'hotel

Pour les appelés on va dans `app.component.ts`,on ajoute le private

![hotel2](img/hotel2.PNG)

Ensuite on s'abonne a l'hotel grace au `ngOnInit`

![pipe](img/pipe.PNG)

`subscribe` pour s'inscrire,avant ca on fait un `pipe` ou l'on fait les opérations avec le console.log

On rajoute le `takeUntil` pour détruire l'abonnement pour eviter que lorsque l'on quitte la page on continue a faire des Get

![http](img/http.PNG)

`httpClient` nous vient d'angular donc il faut l'importé dans `app.module.ts`

![module2](img/modul2.PNG)

# Ng For

En angular pour faire des boucles ont utilise `*ngFor` on appel cela une directive, c'est un outil
qui permet d'apporter des 'super pouvoir' au dome.Il existe aussi des directive sans petite étoile.On va 
itérer sur `allRoutes`.On crée une variable `current`.
 

![current](img/current.PNG)

**On peut faire une interpolation avec double accolade `{{ }}` entre les balises mais il faut utiliser les crochets sur 
la propriéte dans une balise**

![menuModel](img/menuModel.PNG)

On doit typer l'objet, et pour cela on crée un nouveau dossier **modeles**  dans app ou l'on met tout nos modéles de typeScript,
on y crée un fichier **menu.modele.ts** 

![allRoute](img/allRoute.PNG)

On creé 2 objet dans `allRoute` l'url sert pour la route et name pour le nom que l'on veut affiche dans la page web.

Tout étant liés on peut ajouter des routes en plus  de `home et à-propos` sur `menu1.component.ts` mais il faut les
rajouté dans `app-routing.module.ts`

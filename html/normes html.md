#Normes HTML

Html est un language _markup_, c'est a dire qu'il explique a l'ordinateur commment affiché la page sur un écran.

```

< !Doctype html>

< html lang="fr">

< head>

       <meta charset="UTF-8">

       < title> Title < /title>

< /head>

< body>

< /body>

< /html>

```


## Balises a connaîtres


`< h1> Titre principale < /h1> (un par page)` 

`< h2> Deuxième titre < /h2> ( h3,h4...)`

`< p> pour les paragraphes < /p>`

`< div id="mots clé > division pour la mise en page < /div>`

`< article class=" mots clé">pour du contenu < /article>`

`< header> l'en-tête </header>`

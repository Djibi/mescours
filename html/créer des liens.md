# Créer des liens

Pour pouvoir naviguer entre des pages d'un même site on crée des liens entre ces pages avec la balise <a>.

Pour une bonne organisation on crée un dossier par page et chaque page a son index.html, et un dossier assets ou l'ont 

met un sous-dossier pour le css et un sous-dossier pour les images.

Pour faire les lien:

`html
<a href="../main/index.html></a>
`

et sur la deuxieme pages:
`html
<a href="../xp/index.html></a>
`
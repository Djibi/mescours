# numbers manipulation
 
 En js pour faire des calcules mathématiques il faut utiliser des outils qui sont dans une librairie appelé Math.

## round
 
Pour arrondire un nombre ex :

`console.log(Math.round(10.9))`= 11  
 
Pour .round de .5 a .9 il arrondi au nombre supérieur.

## floor

Il coupe ce qu'il y a aprés la virgule:

`console.log(Math.floor(10.8))` = 10

## toFixed

**toFixed n'est pas dans la librairie Math**

`console.log(10.7575880.toFixed(3));`= 10.757

Il coupe en fonction du nombre mis entre ( ).
Il ne crée pas**un nombre mais un string** donc si on met + il conprend qu'il y a 2 string ex :
 `console.log(10.757.to.Fixed(3) + 10 ))`= 10.75710 
 
## parse

Pour transformé un string en nombre on utilise la fonction **parse**:

**parseInt** pour parsé un nombre sans virgule et **parseFloat** un nombre a virgule:

`console.log(parseFloat('10.67) + parseInt(10))` = 20.67

`console.log(parseFloat(10.664.toFixed(3)) + 10);` = 20.664

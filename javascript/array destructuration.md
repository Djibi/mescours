# array destructuration


Sert a récuperer des variables dans un tableaux pour leur donner un noms et y accédé rapidement.

```javascript

const tab = ['tata', 'titi','toto','tutu'];

const [var1, var2, var3,var4] = tab;

```

On peut aussi noté :

` const = [,,var3,var4];`

les éspaces entre les virgules représente les var.

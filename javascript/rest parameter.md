# rest parameter

Il permet d'avoir des fonction sans préciser tout les arguments
```javascript

function add(...args) {
    console.log(args);
    return args;
};
add(2, 3, 4, 5)

```
la console affiche [2, 3, 4, 5]

```javascript

function add(message, ...args ){
    const result = args.reduce((acc, value) => acc + value, 0 );    
    console.log(message, result);
}
add( 'le resultat est :', 2, 3, 4, 5);

```
la console affiche : `le resultat est 14`
On met l'argument rest parameter en dernier quand on a plusieur argument qu'on connait les premiers et pas les derniers.   

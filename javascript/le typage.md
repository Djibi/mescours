# le typage

Le typage sert a présicer de quel type sont les objets qu'on manipulent(string, number, boolean)
on fait du typscript de node mais il y a plusieur type de typscript(veut dire de script).
On crée un dossier **modeles**(c'est une normes)
on y crée autant de fichier interface qu'on a d'objet:
  
### la page serveur.js
```javascript

const company: CompanyModel  = {
    name: '',
    address: '',
    size: 0,
    users:[],
}
let users: UserModel[] = [
     {
    id: 1,
    lastName: 'toto';
    age: 34,
    }, 
    {
    id : 2,
    lastName: 'toto';
    age: 34,
    },
    {
    id :3,
    lastName: 'toto';
    age: 34,
    },
];

```

on crée un fichier **company.modele.ts**(modéle sans 's')
 
```javascript

export interface CompanyModel {
    name: string;  
    address:string;
    size: number;
    users: UserModel[];
}

```
on crée un fichier **user.modele.ts**(user sans 's')

```javascript

export interface UserModel { // on met toujour une majuscule c'est la norme
    id: number;
    lastName: string;
    age: number; 
}

```
**on commence toujour par faire le fichier le plus profond**

```javascript

function getUserById(id: number): UserModel | undefined {
    return users.find((user) => user.id === id)

console.log(getUserById(2)
console.log(getUserById((5467)

```
Avec cette fonction ont retourn l'utilisateur qu'on a choisi

la console affiche ` {id : 2,lastName: 'toto';age: 34,},`
 
la console affiche ` undefined` 
 
`UserModel | undefined` le | veut dire ou en ts.

# les classes

Une classe resemble un peu a un objet on y met des propriétes mais sa a en plus un `construtor`

`constructor` est un mots réserver a un instance d'un classe on ne peut pas appelé une fonction constructor même si constructor

est une fonction.Une classe est composé de plusieur instance.Pour crée une instance on utilise le mot clé `new`

![class](img/class.PNG)

![instance](img/instance.PNG)

On crée une instance de la classe et on la stock dans `myHotel`.Donc `myHotel` devient l'instance de la classe

Dans index.ts:
![newid](img/newid.PNG)

Dans hotel.service.ts
![export-class](img/export-class.PNG)

on a mis `(id)` donc quand on va appelé le constructor il faudra qu'on ai une valeure dans `new HotelClass()`

 `this` est la future instance de la classe 

On obtient
 ![teste](img/teste.PNG)
 
![constructor](img/constructor.PNG)

Quand il y a une donné qu'on ne veut pa utiliser on met ? mais cette valeur doit etre en dernier sinon elle met celle qui sont
sont en bas en rouge 
![pool](img/pool.PNG)

   **2éme soluion**
   
![class2](img/classe2.PNG)

![app.class2](img/app.class2.PNG)

En js quand on fait une classe pas plus de 4 argument a une fonction sinon il faut utiliser `this` et non `constructor`

## les methodes
Ce sont des fonctions spéciales qui servent a manipuler des classes

exemple si ont veut mettre a jour la valeur d'une roomNumber

![app.calculate](img/app.calculate.PNG)

la fonction calculateRoomNumber va d'éclenché le calcule et le this.roomNumber par this.rooms.lenght

![constructor](img/constructor-calculate.PNG)

![resultat-calculate](img/resultat-calculate.PNG)

pour avoir un calcule plus propre:

![classe-propre](img/classe-propre.PNG)

On retourne un hotelCla  ss c'est a dire notre propre instance
 
![classe-propre2](img/calcule-propre2.PNG)

![resultat-classepropre](img/resultat-classepropre.PNG)

## l'heritage

Dans notre exemple on veut des sous-classe un hotel pour riche et un hotel

pour les pauvres:

pour plus de lisibilité on met class a la fin de chaque classe mais ce n'est pas obligatoire
et doit avoir une majuscule comme une interface.

on crée une interface entre luxHotelModel et HotelModel grace au mot clé `extends` pour récuperer les infos
de l'hotel parent et y ajouté les infos de l'hotel de luxe

![luxhotel](img/luxhotel.PNG)


![luxhotel](img/luxhotel2.PNG)
on ajoute `|| 1` pour qu'il doit avoir au minimume une étoile et une terasse 

![app.hoteldeluxe](img/app.hoteldeluxe.PNG)

![resultlux](img/resultlux.PNG)

On fait la même chose avec roomModel en faisant des classes:

`implements` force a avoir le même modéle que RoomModel pour eviter que les dév qui passe aprés sache que il modifient

l'un il faudra qu'il modifie l'autre.

pour generer le constructor: clique droit sur RoomClass=>generate

![appRoomClass](img/appRoomClass.PNG)

![roomClass](img/roomClass.PNG)

![resultRoomClass](img/resultRoomClass.PNG)

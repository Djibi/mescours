# Prédicats

Ce sont des valeurs qui permettent de savoir si une valeur est 'true' ou 'fausse'.

```javascript
const me = {
             firstName : 'mike',
             age: 15,
};

const isMajor = me.age >= 18;

const isBasil = me.firstName === 'basil';

console.log(isMajor);

console.log(isBasil);

```


= :pour une assignation
 
=== :pour une comparaison

&& : pour verifier si 2 conditons sont 'true'
|| : pour verifier si au moins une des conditions est 'true'



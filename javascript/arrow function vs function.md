# Arrow function vs function
Une function est une machine qui prend en entrée des informations les traites 
et renvoi des info en sortie.
On peut aussi avoir des fonction qui n'ont pas d'infos en entrée c'est a dire sans paramétre.
```javascript

function myFunction();

```
Il y a deux facons d'écrire une fonction:

```javascript

function testfunction(a,b) {
    return a + b;
}

```


```javascript

const testfunction2 = (a, b) =>  a + b;

```

On se sert de arrrow function pour les fonction de call back.

Si on écrit l'arrow fonction sur une seul ligne ont enléve return et {}

La différence entre les deux c'est qu'on peut utilisé une fonction
avant de l'avoir déclarer mais ont peut pas utiliser une arrow function avant de l'avoir déclarer
car elle est dans une const. 
```javascript

const testfunction2 = (a, b) =>  a + b;
const result = testfunction(2, 3);

//----------------en haut valide en bas invalide---------------------------
const result = testfunction(2, 3);
const testfunction2 = (a, b) =>  a + b;

```




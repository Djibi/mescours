# boucle for

Pour générer des tableaux on fait des boucles . Entre les crochets ont met une action qu'on repete plusieur fois.
```javascript
for(let i = 0; i < 20; i + 1) {
console.log('toto' + i);
}

```
La console affiche 20 fois toto.

for (initialisateur;la condition de continuation; execution)

initialisateur : on met i pour parcourir un index mais on peut aussi y mettre une fonction ou un bout de code.

execution : tant que la condition de continuation est valide ont continue
 
 Autre facons d'écrire:
 ```javascript
for (; i <= 20;) {
console.log('toto' + i);
 i = i + 1;
}
```

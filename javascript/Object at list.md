# objets as list

c'est un objet dont les clés décrive plusieur chose différente, donc ont peut mettre des objets dans des objets.

```javascript
const Hotel = {
  hotel1:{
    id:1,
    name:'hotel rose',
    roomNumbers : 10,
    pool : true,
    rooms:
        {
           room1:{
             rooName:'suite de luxe',
             size:2,
             id:1,
           },
         },
     },
};

```
## object keys and values.

On prend les objets et on en fait des array. 

```javascript

const rooms = Object.values(hostels.hotel1.rooms);
console.log(rooms);

rooms.forEach((room) => console.log(room));

```

`Object.keys()` renvoi les clés sous forme de tableaux.

`Object.values()` renvoi les valeurs sous forme de tableaux.

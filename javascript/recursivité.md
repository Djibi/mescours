## La récursivité

C'est l'appel d'une fonction a l'intérieur d'une fonction pour l'arreter en cas de besoin et éviter d'afficher plusieur fois le même nombre.

```javascript

const alreadyUsedNumbers = []; 
function randomIntFromInterval(min, max) {  
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function generateUid(min, max) {
  const temp = randomIntFromInterval(min, max);
  if(alreadyUsedNumbers.length === max) {
    return;
  }
if(alreadyUsedNumbers.find((value) => value === temp)) {
  return generateUid(min, max);
  } else {
    alreadyUsedNumbers.push(temp);
    return temp;
  }
}

```
On verifie a chaque fois :
- qu'on a pas atteint la longueur max du tableaux
- que le nombre n'est pas dejas dans le tableaux 
- et ensuite on le push dans le tableaux.

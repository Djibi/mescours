# math random
  
**Math.random** permet de crée des nombres aléatoire.
 
 `console.log(Math.random() * 1000 + 1);` = donne un nombre entre 1 et 1000
 
 en math le * gagne sur le +   

`console.log(Math.floor(Math.random() * 10));` = donne un nombre entier entre 0 et 10.

## random entre 2 nombre

```javascript
function createRandomNumber(min, max) {
return Math.floor(min + Math.random() * (max - min) + 1)
}
console.log(createRandomNumber(10, 15));

```
Cette fonction permet de crée des nombres aléatoires*
 entre 10 et 15

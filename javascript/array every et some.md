# every et some

Il s' applique a un array et renvoi un booléan true ou false

```javascript

const allHostelHavePools = hostels.every(hotel => hostel.pool === true);

console.log( allHostelHavePools);

``` 
Dans notre expemple every renvoi 'false' car il y a un hotel sans piscine.


Some c'est le contraire si il y en a au moins un seul qui a une piscine il renvoi true.


```javascript

const allHostelHavePools = hostels.some(hotel => hostel.pool === true);

console.log( allHostelHavePools);


```

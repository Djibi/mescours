# object destructuration

```javascript

const user = { 
firstName: 'kev',
lastName : 'max',
age : 34,
billionaire :false,
eyes : 'black'
};
const {} = user;

console.log(age, lastName);

```

On créer d'abord une variable avec des {} vides.
On peut ensuite y mettre les clé a éxtraire. 


## object attribute access

```javascript

const user = { 
firstName: 'kev',
lastName : 'max',
age : 34,
billionaire :false,
eyes : 'black'
};

const attrs = ['eyes', 'age'];

attrs.forEach(attr => console.log(user[attr]));

```

La console va afficher black et 34. On ne peut mettre que des strings.
